<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "spring" uri = "http://www.springframework.org/tags" %>
<%@ taglib uri='http://java.sun.com/portlet' prefix='portlet'%>


<portlet:defineObjects/>
<jsp:include page="title.jsp" flush="true" />

<portlet:actionURL var="exit">
	<portlet:param name="_eventId" value="exit" />
	<portlet:param name="execution" value="${flowExecutionKey}" />
</portlet:actionURL>
<p>
	<a href="${exit}">exit</a>
</p>
<p>
	<strong>c:url</strong>: <c:url value="${flowExecutionUrl}" /><br />
	<strong>portlet:actionURL</strong>: <portlet:actionURL>
		<portlet:param name="execution" value="${flowExecutionKey}"/>
	</portlet:actionURL><br />
	<strong>flowExecutionUrl</strong>: ${flowExecutionUrl}
</p>

