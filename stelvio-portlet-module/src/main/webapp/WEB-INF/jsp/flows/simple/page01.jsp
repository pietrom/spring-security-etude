<%@page language="java" pageEncoding="UTF-8"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "spring" uri = "http://www.springframework.org/tags" %>
<%@ taglib uri='http://java.sun.com/portlet' prefix='portlet'%>
<%@ taglib uri="http://displaytag.sf.net/el" prefix='display'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<p>Simbolo dell'Euro in una portlet: €</p>
<portlet:defineObjects/>
showTable = ${showTable}
<c:choose>
	<c:when test="${! empty showTable}">
		<p>
			<span style="font-weight: bold">${flowExecutionUrl}</span>
			<display:table uid="values" name="_values" pagesize="5" 
				requestURI="${flowExecutionUrl}&x=y"
				excludedParams="submitAllInOne showBody ___lit_dt_reload"
			>
				<display:column sortProperty="id" sortable="true" titleKey="id">
					<c:set var="tmpVar">#.00</c:set>
					[${values.id} / ${fn:length(_values)}]
				</display:column>
				<display:column property="description" sortable="true" />
			</display:table>
		</p>
	</c:when>
	<c:otherwise>
		<p>FLOW: simple</p>
		<p>Pagina: 1</p>
		<portlet:actionURL var="next">
			<portlet:param name="_eventId" value="next" />
			<portlet:param name="execution" value="${flowExecutionKey}" />
		</portlet:actionURL>
		<p>
			<a href="${next}">NEXT</a>
		</p>
		<portlet:actionURL var="sub">
			<portlet:param name="_eventId" value="sub" />
			<portlet:param name="execution" value="${flowExecutionKey}" />
		</portlet:actionURL>
		<p>
			<a href="${sub}">SUB</a>
		</p>
		
		<c:if test="${! empty xxx}">
		<p>
			<strong><c:out value='${xxx}' /></strong> 
		</p>
		</c:if>
		
		<c:set var="showTable" scope="request" value="true" />
		<jsp:include page="page01.jsp" flush="true" />
	</c:otherwise>
</c:choose>



		
		
		
		