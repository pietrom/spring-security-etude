<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "spring" uri = "http://www.springframework.org/tags" %>
<%@ taglib uri='http://java.sun.com/portlet' prefix='portlet'%>
<portlet:defineObjects/>
	
		<p>FLOW: simple</p>
		<p>Pagina: 3</p>
		<c:if test="${! empty myMessage}">
		<p>
			<strong><c:out value='${myMessage}' /></strong> 
		</p>
		</c:if>
		<p>
			<code><u>session</u> = <c:out value='${session}' /></code>
		</p>
		<portlet:actionURL var="exec01">
			<portlet:param name="_eventId" value="exec01" />
			<portlet:param name="execution" value="${flowExecutionKey}" />
		</portlet:actionURL>
		<portlet:actionURL var="exec02">
			<portlet:param name="_eventId" value="exec02" />
			<portlet:param name="execution" value="${flowExecutionKey}" />
		</portlet:actionURL>
		<portlet:actionURL var="prev">
			<portlet:param name="_eventId" value="prev" />
			<portlet:param name="execution" value="${flowExecutionKey}" />
		</portlet:actionURL>
		<p>
			<a href="${exec01}">EXEC 01</a>&nbsp;&nbsp;<a href="${exec02}">EXEC 02</a>
		</p>
		<p>
			<a href="${prev}">PREV</a>
		</p>
		<p>
			Current time is <strong><%= new java.util.Date() %></strong>
		</p>
		
		<c:if test="${! empty xxx}">
		<p>
			<strong><c:out value='${xxx}' /></strong> 
		</p>
		</c:if>