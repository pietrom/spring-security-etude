<%@page import="javax.portlet.PortletRequest"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="portlet" uri="http://java.sun.com/portlet_2_0"%>

<portlet:defineObjects />

<span id="loginstatus">
	Logged in as: <security:authentication property="principal.username"/>
	User-info: [${UserContext.userInfo}]
	Extended-User-info: '${ExtendedContext.extendedInfo}'
</span>