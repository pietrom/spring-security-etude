<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/portlet" prefix="portlet" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Hello page</title>
	</head>
	<body>
		<div style="border: 1px solid black;">
			<jsp:include page="user-info.jsp" flush="true" /><br />
			#${MyCtx.extendedInfo}#
		</div>
		<p>
		Message: ${_message}${_x}
		</p>
		<p>
		It's ${_now} o' clock
		</p>
		<p>
		<a href="<portlet:actionURL><portlet:param name='x' value='123'/></portlet:actionURL>">Clic</a>
		</p>
	</body>
</html>