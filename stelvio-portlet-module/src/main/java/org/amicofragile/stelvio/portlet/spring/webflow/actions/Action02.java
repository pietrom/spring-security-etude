package org.amicofragile.stelvio.portlet.spring.webflow.actions;

import java.util.logging.Logger;

import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.context.portlet.PortletExternalContext;
import org.springframework.webflow.core.collection.SharedAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

public class Action02 extends FormAction
{
	Logger logger = Logger.getLogger("org.amicofragile");
	public Event doAction(RequestContext context)
	{
		logger.info("Action02.doAction()");
		PortletExternalContext externalContext = (PortletExternalContext)context.getExternalContext();
		SharedAttributeMap session = externalContext.getSessionMap();
		session.put("synch", "### MESSAGGIO ###");
		session.put("xxx", "### MESSAGGIO ###");
		//context.getFlowScope().put("session", session);
		return result("done");
	}
}