package org.amicofragile.stelvio.portlet.spring.webflow.data;

import java.io.Serializable;

public class Value implements Serializable {
	private final int id;
	private final String description;

	public Value(int id, String description) {
		super();
		this.id = id;
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public String getDescription() {
		return description;
	}
}
