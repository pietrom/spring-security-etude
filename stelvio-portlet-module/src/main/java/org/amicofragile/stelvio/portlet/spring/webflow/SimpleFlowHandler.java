package org.amicofragile.stelvio.portlet.spring.webflow;

import org.springframework.webflow.mvc.portlet.AbstractFlowHandler;

public class SimpleFlowHandler extends AbstractFlowHandler {
	@Override
	public String getFlowId() {
		return "simple";
	}
}
