package org.amicofragile.stelvio.portlet.spring.webflow.actions;

import java.util.logging.Logger;

import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.context.portlet.PortletExternalContext;
import org.springframework.webflow.core.collection.SharedAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

public class Action01 extends FormAction
{
	private static int count = 0;
	Logger logger = Logger.getLogger("org.amicofragile");
	public Event doAction(RequestContext context)
	{
		logger.info("Action01.doAction()");
		context.getFlowScope().put("myMessage", "Numero chiamate: " + (++count));
		PortletExternalContext externalContext = (PortletExternalContext)context.getExternalContext();
		SharedAttributeMap session = externalContext.getSessionMap();
		session.put("synch", new Integer(count));
		session.put("xxx", new Integer(count));
		return result("done");
	}
}