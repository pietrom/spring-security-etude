package org.amicofragile.stelvio.portlet.spring;

import java.util.Date;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletSession;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.mvc.AbstractController;


public class WelcomeController extends AbstractController {

	@Override
	public void handleActionRequest(ActionRequest request, ActionResponse response) throws Exception {
		if(request.getParameter("x") != null) {
			response.setRenderParameter("_x", request.getParameter("x"));
		}
	}

	@Override
	public ModelAndView handleRenderRequest(RenderRequest request, RenderResponse response) throws Exception {
		ModelAndView mav = new ModelAndView("welcome");
        mav.addObject("_message", "Hello, World !");
        mav.addObject("_now", new Date());
        if(request.getParameter("_x") != null) {
        	mav.addObject("_x", request.getParameter("_x"));
        }
        request.getPortletSession().setAttribute("MyCtx", request.getPortletSession().getAttribute("ExtendedContext"), PortletSession.APPLICATION_SCOPE);
        return mav;
	}
}
