package org.amicofragile.stelvio.portlet.spring.webflow.actions;


import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import org.amicofragile.stelvio.portlet.spring.webflow.data.Value;
import org.springframework.webflow.action.FormAction;
import org.springframework.webflow.context.portlet.PortletExternalContext;
import org.springframework.webflow.core.collection.SharedAttributeMap;
import org.springframework.webflow.execution.Event;
import org.springframework.webflow.execution.RequestContext;

public class Init extends FormAction {
	Logger logger = Logger.getLogger("org.amicofragile");

	public Event doAction(RequestContext context) {
		logger.info("Init.doAction()");
		context.getFlowScope().put("_values", buildValuesList());
		return result("done");
	}

	private List<Value> buildValuesList() {
		final LinkedList<Value> values = new LinkedList<Value>();
		values.add(new Value(1, "Uno"));
		values.add(new Value(2, "Due"));
		values.add(new Value(3, "Tre"));
		values.add(new Value(4, "Quattro"));
		values.add(new Value(5, "Cinque"));
		values.add(new Value(6, "Sei"));
		values.add(new Value(7, "Sette"));
		values.add(new Value(8, "Otto"));
		values.add(new Value(9, "Nove"));
		return values;
	}
}