package org.amicofragile.stelvio.auth.fethryduck;

import javax.servlet.http.HttpServletRequest;

import org.amicofragile.stelvio.auth.PreAuthenticationSource;

public class FDuckIncPreAuthSource implements PreAuthenticationSource {

	@Override
	public String readPrincipal(HttpServletRequest request) {
		return request.getHeader("x-fethry-duck-username");
	}
}
