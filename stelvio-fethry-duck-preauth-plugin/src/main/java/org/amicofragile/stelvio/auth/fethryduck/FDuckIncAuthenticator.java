package org.amicofragile.stelvio.auth.fethryduck;

import java.util.Arrays;
import java.util.Collection;

import org.amicofragile.stelvio.auth.AuthenticationFailedException;
import org.amicofragile.stelvio.auth.Authenticator;

public class FDuckIncAuthenticator implements Authenticator {

	@Override
	public Collection<String> doAuthentication(String username, String password) throws AuthenticationFailedException {
		if("fethry".equals(username)) {
			return Arrays.asList(new String[] {"ROLE_USER", "FD_INC_POWER_USER", "FD_INC_USER"});
		}
		throw new AuthenticationFailedException("Authentication failed for username " + username);
	}

}
