package org.amicofragile.stelvio.auth.fethryduck;

import org.amicofragile.stelvio.auth.PreAuthenticationSource;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;

public class FDuckIncPreAuthSourceTest {
	@Test
	public void givenRequestWithPreAuthHeaderThenReturnsNotNullPrincipal() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		request.addHeader("x-fethry-duck-username", "fethry");
		PreAuthenticationSource source = new FDuckIncPreAuthSource();
		Assert.assertEquals("fethry", source.readPrincipal(request));
	}
	
	@Test
	public void givenRequestWithoutPreAuthHeaderThenReturnsNullPrincipal() {
		MockHttpServletRequest request = new MockHttpServletRequest();
		PreAuthenticationSource source = new FDuckIncPreAuthSource();
		Assert.assertNull(source.readPrincipal(request));
	}
}
