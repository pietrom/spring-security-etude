package org.amicofragile.stelvio.auth.fethryduck;

import java.util.Collection;

import junit.framework.Assert;

import org.amicofragile.stelvio.auth.AuthenticationFailedException;
import org.amicofragile.stelvio.auth.Authenticator;
import org.junit.Test;

public class FDuckIncAuthenticatorTest {
	@Test(expected=AuthenticationFailedException.class)
	public void shouldFailWhenUsernameIsNotValid() throws AuthenticationFailedException {
		Authenticator authenticator = new FDuckIncAuthenticator();
		authenticator.doAuthentication("xyz", "");
	}
	
	@Test
	public void shouldReturnFourRolesForValidUsername() throws AuthenticationFailedException {
		Authenticator authenticator = new FDuckIncAuthenticator();
		final Collection<String> roles = authenticator.doAuthentication("fethry", "");
		Assert.assertEquals(3, roles.size());
	}
}
