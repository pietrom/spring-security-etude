package org.amicofragile.stelvio.acceptance.stories;

import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class LogoutAcceptanceTest extends BaseUserStory {
	@Test
	public void shouldBeAbleToLogoutOfApplicationAndSeeMessageThatConfirmsThis() {
		get(HOME_PAGE);
		login(GUEST_USERNAME, GUEST_PASSWORD);
		assertThat(driver.getTitle(), is(HOME_PAGE_TITLE));
		clickOn(By.linkText("Logout"));
		assertThat(driver.getTitle(), is(LOGIN_PAGE_TITLE));

		final WebElement informationMessageSection = driver.findElement(By.id("infomessage"));
		assertThat(informationMessageSection.getText(), containsString("You have been successfully logged out."));
	}
}
