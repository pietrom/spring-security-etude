package org.amicofragile.stelvio.acceptance.stories;

import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class HomePageAfterLoginAcceptanceTest extends BaseUserStory {
	@Test
	public void shouldBeAbleToViewHomePageWhenSuccessfullyAuthenticatedWithStandardUserCredentials() {
		get(LOGIN_PAGE);
		assertThat(driver.getTitle(), is(LOGIN_PAGE_TITLE));
		login(GUEST_USERNAME, GUEST_PASSWORD);
		assertThat(driver.getTitle(), is(HOME_PAGE_TITLE));
	}
	
	@Test
	public void shouldBeAbleToViewHomePageWhenSuccessfullyAuthenticatedWithAdminUserCredentials() {
		get(LOGIN_PAGE);
		assertThat(driver.getTitle(), is(LOGIN_PAGE_TITLE));
		login(ADMIN_USERNAME, ADMIN_PASSWORD);
		assertThat(driver.getTitle(), is(ADMIN_PAGE_TITLE));
	}
}
