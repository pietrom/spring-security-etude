package org.amicofragile.stelvio.acceptance.stories;

import org.junit.Test;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.*;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;

public class AdminPageOnlyForAdminsAcceptanceTest extends BaseUserStory {
	@Test
	public void shouldReceive403ForbiddenWhenAccessingAdminPageWithoutAdminRole() {
		get(ADMIN_PAGE);
		login(GUEST_USERNAME, GUEST_PASSWORD);
		assertThat(driver.getTitle(), is(not(ADMIN_PAGE_TITLE)));
		assertThat(driver.getPageSource(), containsString("403"));
	}
}
