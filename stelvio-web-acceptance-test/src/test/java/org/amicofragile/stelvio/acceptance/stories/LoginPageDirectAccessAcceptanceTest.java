package org.amicofragile.stelvio.acceptance.stories;

import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.GUEST_PASSWORD;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.GUEST_USERNAME;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.HOME_PAGE;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.HOME_PAGE_TITLE;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.LOGIN_PAGE_TITLE;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class LoginPageDirectAccessAcceptanceTest extends BaseUserStory {
	@Test
	public void shouldBeAbleToViewHomePageWhenSuccessfullyAuthenticated() {
		get(HOME_PAGE);
		assertThat(driver.getTitle(), is(LOGIN_PAGE_TITLE));
		login(GUEST_USERNAME, GUEST_PASSWORD);
		assertThat(driver.getTitle(), is(HOME_PAGE_TITLE));
	}
}
