package org.amicofragile.stelvio.acceptance.stories;

import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.ADMIN_PAGE;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.ADMIN_PASSWORD;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.ADMIN_USERNAME;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.GUEST_PASSWORD;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.GUEST_USERNAME;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.HOME_PAGE;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;

import org.junit.Test;
import org.openqa.selenium.By;

public class DisplayLoggedUserInfoAcceptanceTest extends BaseUserStory {
	@Test
	public void shouldBeAbleToViewLoggedInUsernameOnAdminPage() {
		get(ADMIN_PAGE);
		login(ADMIN_USERNAME, ADMIN_PASSWORD);
		assertThat(driver.findElement(By.id("loginstatus")).getText(), containsString("Logged in as: admin"));
	}

	@Test
	public void shouldBeAbleToViewLoggedInUsernameOnHomePage() {
		get(HOME_PAGE);
		login(GUEST_USERNAME, GUEST_PASSWORD);
		assertThat(driver.findElement(By.id("loginstatus")).getText(), containsString("Logged in as: guest"));
	}
	
	@Test
	public void shouldBeAbleToViewLoggedInUserInfoOnHomePage() {
		get(HOME_PAGE);
		login(GUEST_USERNAME, GUEST_PASSWORD);
		assertThat(driver.findElement(By.id("loginstatus")).getText(), containsString("User-info: [GUEST]"));
		assertThat(driver.findElement(By.id("loginstatus")).getText(), containsString("Extended-User-info: [guest:ROLE_USER]"));
	}
	
	@Test
	public void shouldBeAbleToViewLoggedInUserInfoOnAdminPage() {
		get(HOME_PAGE);
		login(ADMIN_USERNAME, ADMIN_PASSWORD);
		assertThat(driver.findElement(By.id("loginstatus")).getText(), containsString("User-info: [ADMIN]"));
		assertThat(driver.findElement(By.id("loginstatus")).getText(), containsString("Extended-User-info: [admin:ROLE_ADMIN,ROLE_USER]"));
	}
}
