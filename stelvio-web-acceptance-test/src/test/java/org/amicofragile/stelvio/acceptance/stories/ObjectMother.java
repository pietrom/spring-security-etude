package org.amicofragile.stelvio.acceptance.stories;

public class ObjectMother {
	public static final String ROOT_PAGE = "/";
	
	public static final String LOGIN_PAGE = "/login.jsp";
	public static final String LOGIN_PAGE_TITLE = "Login: stelvio webapp";
	public static final String HOME_PAGE = "/index.jsp";
	public static final String HOME_PAGE_TITLE = "stelvio-web-module main page";
	public static final String ABOUT_PAGE = "/about.jsp";
	public static final String ABOUT_PAGE_TITLE = "stelvio about and credits page";
	public static final String ADMIN_PAGE = "/admin.jsp";
	public static final String ADMIN_PAGE_TITLE = "Admin: Spring Security Web Application";
	
	public static final String GUEST_USERNAME = "guest";
	public static final String GUEST_PASSWORD = "guestpass";
	public static final String ADMIN_USERNAME = "admin";
	public static final String ADMIN_PASSWORD = "adminpass";
	
}
