package org.amicofragile.stelvio.acceptance.stories;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

public abstract class BaseUserStory {
	protected WebDriver driver;
	
	protected void use(WebDriver session) {
		this.driver = session;
	}
	
	@Before
	public void openBrowser() {
		driver = new HtmlUnitDriver();
	}
	
	@After
	public void logoutAndCloseTheBrowserAfterEachTest() {
		logout();
		driver.close();
	}
	
	protected void get(String path) {
		driver.get("http://localhost:8080/stelvio-web-module" + path);
	}
	
	protected void clickOn(By by) {
		final WebElement btn = driver.findElement(by);
		btn.click();
	}
	
	protected void setFieldValue(By selector, String value) {
		final WebElement field = driver.findElement(selector);
		field.sendKeys(value);
	}
	
	protected void login(String username, String password) {
		setFieldValue(By.name("j_username"), username);
		setFieldValue(By.name("j_password"), password);
		clickOn(By.name("submit"));
	}
	
	protected void logout() {
		get("/logout");
	}
}
