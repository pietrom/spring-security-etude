package org.amicofragile.stelvio.acceptance.stories;

import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.GUEST_PASSWORD;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.GUEST_USERNAME;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.HOME_PAGE;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.LOGIN_PAGE;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.LOGIN_PAGE_TITLE;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SingleSessionAcceptanceTest extends BaseUserStory {
	private WebDriver secondSession;

	@Test
	public void shouldOnlyAllowOneConcurrentSessionPerUser() throws Exception {
		get(LOGIN_PAGE);
		login(GUEST_USERNAME, GUEST_PASSWORD);
		// close browser session
		WebDriver oldSession = driver;
		secondSession = new FirefoxDriver();
		use(secondSession);
		get(LOGIN_PAGE);
		// login
		login(GUEST_USERNAME, GUEST_PASSWORD);
		use(oldSession);
		get(HOME_PAGE);
		assertThat(driver.getTitle(), is(LOGIN_PAGE_TITLE));
	}

	@After
	public void closeSecondSession() {
		secondSession.close();
	}
}
