package org.amicofragile.stelvio.acceptance.stories;

import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.GUEST_PASSWORD;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.GUEST_USERNAME;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.LOGIN_PAGE;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.LOGIN_PAGE_TITLE;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class AuthenticationFailureAcceptanceTest extends BaseUserStory {
	@Test
	public void shouldRemainOnLoginPageWithInformativeMessageWhenAuthenticationFailsDueToIncorrectPassword() {
		get(LOGIN_PAGE);
		login(GUEST_USERNAME, "wrongguestpass");
		assertThat(driver.getTitle(), is(LOGIN_PAGE_TITLE));
		final WebElement informationMessageSection = driver.findElement(By.id("infomessage"));
		assertThat(informationMessageSection.getText(),	containsString("Login failed due to: Bad credentials."));
	}

	@Test
	public void shouldRemainOnLoginPageWithInformativeMessageWhenAuthenticationFailsDueToIncorrectUsername() {
		get(LOGIN_PAGE);
		login("wrongguest", GUEST_PASSWORD);
		assertThat(driver.getTitle(), is(LOGIN_PAGE_TITLE));
		final WebElement informationMessageSection = driver.findElement(By.id("infomessage"));
		assertThat(informationMessageSection.getText(),	containsString("Login failed due to: Could not find user: wrongguest."));
	}

	@Test
	public void shouldRemainOnLoginPageWithUsernameStillPopulatedWhenAuthenticationFails() {
		get(LOGIN_PAGE);
		login("theguest", "thepass");
		assertThat(driver.getTitle(), is(LOGIN_PAGE_TITLE));
		final WebElement username = driver.findElement(By.name("j_username"));
		assertThat(username.getAttribute("value"), is("theguest"));
	}
}
