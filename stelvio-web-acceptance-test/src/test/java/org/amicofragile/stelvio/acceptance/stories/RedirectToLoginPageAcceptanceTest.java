package org.amicofragile.stelvio.acceptance.stories;


import static org.hamcrest.CoreMatchers.is;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.*;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class RedirectToLoginPageAcceptanceTest extends BaseUserStory {
	@Test
	public void shouldBeAskedToAuthenticateWhenTryingToVisitASecurePageAndNotLoggedIn() {
		get(HOME_PAGE);
		assertThat(driver.getTitle(), is(not(HOME_PAGE_TITLE)));
		assertThat(driver.getTitle(), is(LOGIN_PAGE_TITLE));
	}
}
