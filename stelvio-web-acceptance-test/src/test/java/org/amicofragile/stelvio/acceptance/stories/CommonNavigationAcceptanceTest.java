package org.amicofragile.stelvio.acceptance.stories;

import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.*;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import org.junit.Test;
import org.openqa.selenium.By;

public class CommonNavigationAcceptanceTest extends BaseUserStory {
	@Test
	public void shouldBeAbleToSeeCommonNavigationOnAdminPage() {
		get(ADMIN_PAGE);
		login(ADMIN_USERNAME, ADMIN_PASSWORD);

		assertThat(driver.getTitle(), is(ADMIN_PAGE_TITLE));
		assertNotNull(driver.findElement(By.linkText("Home")));
		assertNotNull(driver.findElement(By.linkText("Admin")));
		assertNotNull(driver.findElement(By.linkText("Logout")));
	}

	@Test
	public void shouldBeAbleToSeeCommonNavigationOnHomePage() {
		get(HOME_PAGE);
		login(GUEST_USERNAME, GUEST_PASSWORD);
		assertThat(driver.getTitle(), is(HOME_PAGE_TITLE));
		assertNotNull(driver.findElement(By.linkText("Home")));
		try {
			driver.findElement(By.linkText("Admin"));
			fail("should not be able to see a link to admin page when logged in as standard user");
		} catch (final Exception e) {
			assertNotNull(e);
		}
		assertNotNull(driver.findElement(By.linkText("Logout")));
	}
}
