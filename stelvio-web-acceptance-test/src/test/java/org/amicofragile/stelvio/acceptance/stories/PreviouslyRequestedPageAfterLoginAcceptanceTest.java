package org.amicofragile.stelvio.acceptance.stories;

import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.ABOUT_PAGE;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.ABOUT_PAGE_TITLE;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.GUEST_PASSWORD;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.GUEST_USERNAME;
import static org.amicofragile.stelvio.acceptance.stories.ObjectMother.LOGIN_PAGE_TITLE;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Test;

public class PreviouslyRequestedPageAfterLoginAcceptanceTest extends BaseUserStory {
	@Test
	public void shouldBeAbleToViewPreviouslyRequestedPageWhenSuccessfullyAuthenticated() {
		get(ABOUT_PAGE);
		assertThat(driver.getTitle(), is(LOGIN_PAGE_TITLE));
		login(GUEST_USERNAME, GUEST_PASSWORD);
		assertThat(driver.getTitle(), is(ABOUT_PAGE_TITLE));
	}
}
