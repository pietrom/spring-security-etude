package org.amicofragile.stelvio.auth.goofy;

import java.util.Arrays;
import java.util.Collection;

import org.amicofragile.stelvio.auth.AuthenticationFailedException;
import org.amicofragile.stelvio.auth.Authenticator;

public class GoofyAuthenticator implements Authenticator {

	@Override
	public Collection<String> doAuthentication(String username, String password) throws AuthenticationFailedException {
		if("Celery_will_you".equals(username)) {
			return Arrays.asList(new String[] {"ROLE_ADMIN", "ROLE_USER", "ROLE_GOOFY_POWER_USER", "ROLE_GOOFY_USER"});
		}
		throw new AuthenticationFailedException("Invalid pre-authenticated GoofyInc's user");
	}
}
