package org.amicofragile.stelvio.auth.goofy;

import javax.servlet.http.HttpServletRequest;

import org.amicofragile.stelvio.auth.PreAuthenticationSource;

public class GoofyIncPreAuthenticationSource implements PreAuthenticationSource {
	@Override
	public String readPrincipal(HttpServletRequest request) {
		return request.getParameter("open_yourself_celery");
	}
}
