package org.acme.auth;

import java.util.Arrays;
import java.util.Collection;

public class AuthenticationFacade {
	public static void main(String[] args) {
		System.out.println("org.acme.auth:acme-auth-client - Hello World!");
	}

	public static Collection<String> login(String username, String password) throws AuthenticationException {
		if ("acmeadmin".equals(username) && "acmepassword".equals(password)) {
			return Arrays.asList(new String[] {"ROLE_ADMIN", "ROLE_USER"});
		}
		throw new AuthenticationException("Authentication failed");
	}
}
