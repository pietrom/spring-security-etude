package org.acme.auth;

public class AuthenticationException extends Exception {
	public AuthenticationException(String message) {
		super(message);
	}
}
