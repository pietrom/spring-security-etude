package org.acme.auth;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Test;

public class AuthenticationFacadeTest {
	@Test
	public void shouldReturnRolesListWhenValidCredentialsAreProvided() throws AuthenticationException {
		Collection<String> roles = AuthenticationFacade.login("acmeadmin", "acmepassword");
		Assert.assertEquals(2, roles.size());
		Assert.assertTrue(roles.contains("ROLE_USER"));
		Assert.assertTrue(roles.contains("ROLE_ADMIN"));
	}
	
	@Test(expected=AuthenticationException.class)
	public void shouldThrowAuthenticationExceptionWhenInvalidCredentialsAreProvided() throws AuthenticationException {
		AuthenticationFacade.login("acmeadmin", "wrongpassword");
	}
}
