<%@taglib uri="http://portlet-container.dev.java.net" prefix="pcdriver"%>
<%@taglib uri="http://java.sun.com/jstl/core_rt" prefix="c"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@page language="java" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<link rel="StyleSheet" href="<c:url value='/css/portal.css' />" type="text/css" />
		<link rel="StyleSheet" href="<c:url context='/LIT-Kebab_Web' value='/css/themes/orange/official/styles.css' />" type="text/css" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	</head>
	<body>
		<hr />
			<jsp:include page="user-info.jsp" flush="true" />
		<hr />
			<pcdriver:portlet portletName="WebflowPortlet" applicationName="stelvio-portlet-module" >
		
			     <div class="portlet-title"><pcdriver:title/></div>
			
			     <pcdriver:render/>
		
		 	</pcdriver:portlet>
		<hr />
			<pcdriver:portlet portletName="SpringPortlet" applicationName="stelvio-portlet-module" >
		
			     <div class="portlet-title"><pcdriver:title/></div>
			
			     <pcdriver:render/>
		
		 	</pcdriver:portlet>
		<hr />
			Clone: <pcdriver:portlet portletName="SpringPortletClone" applicationName="stelvio-portlet-module" >
		
			     <div class="portlet-title"><pcdriver:title/></div>
			
			     <pcdriver:render/>
		
		 	</pcdriver:portlet>
		<hr />
	</body>
</html>