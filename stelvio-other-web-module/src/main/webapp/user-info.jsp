<%@ page language="java" contentType="text/html; charset=UTF-8" isELIgnored="false" %>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<span id="loginstatus">
	Logged in as: <security:authentication property="principal.username"/>
	User-info: [${UserContext.userInfo}]
	Extended-User-info: [${ExtendedContext.extendedInfo}]
</span>