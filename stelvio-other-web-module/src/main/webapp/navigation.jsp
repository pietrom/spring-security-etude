<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix='security' uri='http://www.springframework.org/security/tags' %>
<div id="navcontainer">
<ul id="navlist">
    <li class="home"><a href="index.jsp">Home</a></li>
    <security:authorize ifAllGranted="ROLE_ADMIN">
    <li><a href="admin.jsp">Admin</a></li>
    </security:authorize>
    <li><a href="logout">Logout</a></li>
</ul>
</div>