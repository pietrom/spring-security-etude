<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>stelvio about and credits page</title>
	</head>
	<body>
		<h1>stelvio about and credits page</h1>
		<h2>About:</h2>
		<code>stelvio-spring.security-etude</code>, by Pietro Martinelli
		<h2>Credits:</h2>
		Use cases and sample code by
		<code>http://heraclitusonsoftware.wordpress.com/software-development/spring/simple-web-application-with-spring-security-part-1</code>
	</body>
</html>