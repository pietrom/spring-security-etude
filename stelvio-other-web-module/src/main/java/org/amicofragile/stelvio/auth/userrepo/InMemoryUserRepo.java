package org.amicofragile.stelvio.auth.userrepo;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.dao.DataAccessException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public class InMemoryUserRepo implements UserDetailsService {
	private final Map<String, UserDetails> users;
	public InMemoryUserRepo() {
		this.users = new HashMap<String, UserDetails>();
		users.put("guest", new User("guest", "guestpass", true, true, true, true, Arrays.asList(new GrantedAuthority[] {new GrantedAuthorityImpl("ROLE_USER")})));
		users.put("admin", new User("admin", "adminpass", true, true, true, true, Arrays.asList(new GrantedAuthority[] {new GrantedAuthorityImpl("ROLE_USER"), new GrantedAuthorityImpl("ROLE_ADMIN")})));
	}
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException, DataAccessException {
		if(!users.containsKey(username)) {
			throw new UsernameNotFoundException(String.format("Could not find user: %s.", username));
		}
		return users.get(username);
	}

}
