package org.amicofragile.stelvio.auth;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

public class RoleAwareAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
	private String adminTargetUrl = "/admin";

	@Override
	protected String determineTargetUrl(HttpServletRequest request, HttpServletResponse response) {
		if(containsAdminAuthority(SecurityContextHolder.getContext().getAuthentication())) {
			return adminTargetUrl ;
		}
		return super.determineTargetUrl(request, response);
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws ServletException, IOException {
		super.onAuthenticationSuccess(request, response, authentication);
	}

	private boolean containsAdminAuthority(Authentication authentication) {
		for (final GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
			if (grantedAuthority.getAuthority().equals("ROLE_ADMIN")) {
				return true;
			}
		}
		return false;
	}
	
	public void setAdminTargetUrl(String adminTargetUrl) {
		this.adminTargetUrl = adminTargetUrl;
	}
}
