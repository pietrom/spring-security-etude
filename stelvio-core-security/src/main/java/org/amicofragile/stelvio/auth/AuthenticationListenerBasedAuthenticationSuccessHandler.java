package org.amicofragile.stelvio.auth;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amicofragile.stelvio.security.management.AuthenticationListener;
import org.amicofragile.stelvio.web.servlet.ServletInput;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

public class AuthenticationListenerBasedAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
	private final AuthenticationListener authenticationListener;
	
	public AuthenticationListenerBasedAuthenticationSuccessHandler(AuthenticationListener authenticationListener) {
		this.authenticationListener = authenticationListener;
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		authenticationListener.onAuthenticationSuccess(new ServletInput(request), authentication.getName(), buildRoles(authentication));
	}

	private Collection<String> buildRoles(Authentication authentication) {
		final LinkedList<String> roles = new LinkedList<String>();
		for (GrantedAuthority role : authentication.getAuthorities()) {
			roles.add(role.getAuthority());
		}
		return roles;
	}
}
