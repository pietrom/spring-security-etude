package org.amicofragile.stelvio.auth;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

public class ComposedAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
	private final List<AuthenticationSuccessHandler> handlers;

	public ComposedAuthenticationSuccessHandler(AuthenticationSuccessHandler handler) {
		handlers = new LinkedList<AuthenticationSuccessHandler>();
		handlers.add(handler);
	}

	public ComposedAuthenticationSuccessHandler(List<AuthenticationSuccessHandler> handlers) {
		this.handlers = handlers;
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
		for (AuthenticationSuccessHandler handler : handlers) {
			handler.onAuthenticationSuccess(request, response, authentication);
		}
	}

}
