package org.amicofragile.stelvio.auth;

import java.util.ServiceLoader;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

public class ExternalAuthenticationPreAuthenticatedFilter extends AbstractPreAuthenticatedProcessingFilter {
	private static ServiceLoader<PreAuthenticationSource> preAuthenticationSources = ServiceLoader.<PreAuthenticationSource>load(PreAuthenticationSource.class);
	private AuthenticationSuccessHandler authenticationSuccessHandler;
	
	@Override
	protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {
		for(PreAuthenticationSource source : preAuthenticationSources) {
			String username = source.readPrincipal(request);
			if(username != null) {
				return username;
			}
		}
		return null;
	}

	@Override
	protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
		return "dummy";
	}
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, Authentication authResult) {
		super.successfulAuthentication(request, response, authResult);
		if(this.authenticationSuccessHandler != null) {
			try {
				this.authenticationSuccessHandler.onAuthenticationSuccess(request, response, authResult);
			} catch (Exception e) {
				System.out.println("Error processing authentication success handler");
				e.printStackTrace();
			}
		}
	}
	
	public void setAuthenticationSuccessHandler(AuthenticationSuccessHandler authenticationSuccessHandler) {
		this.authenticationSuccessHandler = authenticationSuccessHandler;
	}
}
