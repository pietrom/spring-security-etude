package org.amicofragile.stelvio.auth;

import java.util.Collection;

public interface Authenticator {
	public abstract Collection<String> doAuthentication(String username, String password) throws AuthenticationFailedException;
}
