package org.amicofragile.stelvio.auth;

import java.io.IOException;
import java.util.Collection;
import java.util.ServiceLoader;

import javax.servlet.ServletException;

import org.amicofragile.stelvio.security.management.AuthenticationListener;
import org.amicofragile.stelvio.web.Input;

public class PluggableAuthenticationListener implements AuthenticationListener {
	private static ServiceLoader<AuthenticationListener> listenersLoader = ServiceLoader.load(AuthenticationListener.class);
	
	@Override
	public void onAuthenticationSuccess(Input input, String username, Collection<String> roles) throws IOException, ServletException {
		for (AuthenticationListener listener : listenersLoader) {
			listener.onAuthenticationSuccess(input, username, roles);
		}
	}
}
