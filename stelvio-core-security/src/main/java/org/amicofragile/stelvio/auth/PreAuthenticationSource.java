package org.amicofragile.stelvio.auth;

import javax.servlet.http.HttpServletRequest;

public interface PreAuthenticationSource {
	public abstract String readPrincipal(HttpServletRequest request);
}
