package org.amicofragile.stelvio.auth;

import java.util.Collection;
import java.util.LinkedList;
import java.util.ServiceLoader;

import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.userdetails.User;

public class PreAuthenticatedAuthenticationProvider implements AuthenticationProvider {
	private static ServiceLoader<Authenticator> authenticators = ServiceLoader.load(Authenticator.class);

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		final String username = authentication.getName();
		final String password = (String) authentication.getCredentials();
		for (Authenticator authenticator : authenticators) {
			try {
				Collection<String> roles = authenticator.doAuthentication(username, password);
				if (roles != null) {
					Collection<GrantedAuthority> authorities = new LinkedList<GrantedAuthority>();
					for(String role : roles) {
						authorities.add(new GrantedAuthorityImpl(role));
					}
					return new UsernamePasswordAuthenticationToken(buildprincipal(username, password, authorities), password, authorities);
				}
			} catch (AuthenticationFailedException afe) {
				System.out.println("No valid authentication found using Authenticator " + authenticator);
			}
		}
		return null;
	}

	private Object buildprincipal(String username, String password, Collection<GrantedAuthority> authorities) {
		return new User(username, password, true, true, true, true, authorities);
	}

	@Override
	public boolean supports(Class<? extends Object> authentication) {
		return true;
	}
}
