package org.amicofragile.stelvio.security.management;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;

import org.amicofragile.stelvio.web.Input;

public interface AuthenticationListener {
	public void onAuthenticationSuccess(Input input, String username, Collection<String> roles) throws IOException, ServletException;
}
