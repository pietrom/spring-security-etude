package org.amicofragile.stelvio.auth;

import java.io.IOException;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

public class ComposedAuthenticationSuccessHandlerTest {
	@Test
	public void invokeSingleHandler() throws IOException, ServletException {
		MockedHandler handler = new MockedHandler();
		ComposedAuthenticationSuccessHandler composedHandler = new ComposedAuthenticationSuccessHandler(handler);
		composedHandler.onAuthenticationSuccess(null, null, null);
		Assert.assertTrue("Single wrapped handler not invoked", handler.isCalled());
	}

	@Test
	public void invokingMultipleHandlers() throws IOException, ServletException {
		MockedHandler[] handlers = new MockedHandler[10];
		for (int i = 0; i < handlers.length; i++) {
			handlers[i] = new MockedHandler();
		}
		ComposedAuthenticationSuccessHandler composedHandler = new ComposedAuthenticationSuccessHandler(Arrays.asList((AuthenticationSuccessHandler[]) handlers));
		composedHandler.onAuthenticationSuccess(null, null, null);
		for (int i = 0; i < handlers.length; i++) {
			Assert.assertTrue("Wrapped handler " + i + "not invoked", handlers[i].isCalled());
		}
	}

	private static final class MockedHandler implements AuthenticationSuccessHandler {
		private boolean called = false;

		@Override
		public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
			called = true;
		}

		public boolean isCalled() {
			return called;
		}
	}
}
