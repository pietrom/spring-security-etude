package org.amicofragile.stelvio.auth;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.GrantedAuthorityImpl;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

public class RoleAwareAuthenticationSuccessHandlerTest {
	private static final String DEFAULT_TARGET_URL = "/default";
	private static final String ADMIN_TARGET_URL = "/adm";

	@Test
	public void shouldReturnAdminTargetUrlWhenAuthenticatedPrincipalIsAdmin() {
		final RoleAwareAuthenticationSuccessHandler handler = new RoleAwareAuthenticationSuccessHandler();
		handler.setDefaultTargetUrl(DEFAULT_TARGET_URL);
		handler.setAdminTargetUrl(ADMIN_TARGET_URL);
		
		SecurityContextImpl securityContext = new SecurityContextImpl();
		securityContext.setAuthentication(buildAuthentication("ROLE_ADMIN"));
		SecurityContextHolder.setContext(securityContext);
		String targetUrl = handler.determineTargetUrl(null, null);
		Assert.assertEquals(ADMIN_TARGET_URL, targetUrl);
	}
	
	@Test
	public void shouldReturnDefaultTargetUrlWhenAuthenticatedPrincipalIsNotAdmin() {
		final RoleAwareAuthenticationSuccessHandler handler = new RoleAwareAuthenticationSuccessHandler();
		handler.setDefaultTargetUrl(DEFAULT_TARGET_URL);
		handler.setAdminTargetUrl(ADMIN_TARGET_URL);
		
		SecurityContextImpl securityContext = new SecurityContextImpl();
		securityContext.setAuthentication(buildAuthentication("ROLE_USER"));
		SecurityContextHolder.setContext(securityContext);
		String targetUrl = handler.determineTargetUrl(new MockHttpServletRequest(), null);
		Assert.assertEquals(DEFAULT_TARGET_URL, targetUrl);
	}

	private TestingAuthenticationToken buildAuthentication(String role) {
		return new TestingAuthenticationToken(null, null, new GrantedAuthority[]{new GrantedAuthorityImpl(role)});
	}
}
