<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Admin: Spring Security Web Application</title>
	</head>
	<body>
		<h1>stelvio admin</h1>
		<jsp:include page="navigation.jsp" flush="true" />
		<jsp:include page="user-info.jsp" flush="true" />
	</body>
</html>