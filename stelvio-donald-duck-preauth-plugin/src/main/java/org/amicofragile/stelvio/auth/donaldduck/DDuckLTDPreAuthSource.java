package org.amicofragile.stelvio.auth.donaldduck;

import javax.servlet.http.HttpServletRequest;

import org.amicofragile.stelvio.auth.PreAuthenticationSource;

public class DDuckLTDPreAuthSource implements PreAuthenticationSource {
	@Override
	public String readPrincipal(HttpServletRequest request) {
		return "donald";
	}
}
