package org.amicofragile.stelvio.auth.donaldduck;

import java.util.Arrays;
import java.util.Collection;

import org.amicofragile.stelvio.auth.AuthenticationFailedException;
import org.amicofragile.stelvio.auth.Authenticator;

public class DDuckLTDAuthenticator implements Authenticator {

	@Override
	public Collection<String> doAuthentication(String username, String password) throws AuthenticationFailedException {
		if ("donald".equals(username)) {
			return Arrays.asList(new String[] {"ROLE_USER", "DD_LTD_POWER_USER", "DD_LTD_USER"});
		}
		throw new AuthenticationFailedException("Authentication failed for username " + username);
	}
}
