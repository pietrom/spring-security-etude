package org.amicofragile.stelvio.auth.donaldduck;

import java.util.Collection;

import junit.framework.Assert;

import org.amicofragile.stelvio.auth.AuthenticationFailedException;
import org.amicofragile.stelvio.auth.Authenticator;
import org.junit.Test;

public class DDuckLTDAuthenticatorTest {
	@Test(expected=AuthenticationFailedException.class)
	public void shouldFailWhenUsernameIsNotDonald() throws AuthenticationFailedException {
		Authenticator authenticator = new DDuckLTDAuthenticator();
		authenticator.doAuthentication("xyz", "");
	}
	
	@Test
	public void shouldReturnFourRolesForUsernameDonald() throws AuthenticationFailedException {
		Authenticator authenticator = new DDuckLTDAuthenticator();
		final Collection<String> roles = authenticator.doAuthentication("donald", "");
		Assert.assertEquals(3, roles.size());
	}
}
