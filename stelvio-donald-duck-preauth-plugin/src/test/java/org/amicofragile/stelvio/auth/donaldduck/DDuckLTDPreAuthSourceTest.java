package org.amicofragile.stelvio.auth.donaldduck;

import org.amicofragile.stelvio.auth.PreAuthenticationSource;
import org.junit.Assert;
import org.junit.Test;

public class DDuckLTDPreAuthSourceTest {
	@Test
	public void shouldReturnFixedPrincipalIgnoringRequest() {
		PreAuthenticationSource source = new DDuckLTDPreAuthSource();
		Assert.assertEquals("donald", source.readPrincipal(null));
	}
}
