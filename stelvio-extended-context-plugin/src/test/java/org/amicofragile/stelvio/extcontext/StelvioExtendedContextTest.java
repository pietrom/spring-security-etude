package org.amicofragile.stelvio.extcontext;

import org.junit.Assert;
import org.junit.Test;

public class StelvioExtendedContextTest {
	@Test
	public void getExtendedInfoReturnsUsernameAndRoles() {
		final String username = "theUsername";
		final StelvioExtendedContext ctx = new StelvioExtendedContext(username);
		ctx.addRole("role2");
		ctx.addRole("role1");
		ctx.addRole("role3");
		Assert.assertEquals(username + ":role1,role2,role3", ctx.getExtendedInfo());
	}
}
