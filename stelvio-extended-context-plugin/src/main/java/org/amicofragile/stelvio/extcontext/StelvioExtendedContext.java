package org.amicofragile.stelvio.extcontext;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class StelvioExtendedContext {
	private final String username;
	private final List<String> roles;
	
	public StelvioExtendedContext(String username) {
		this.username = username;
		this.roles = new LinkedList<String>();
	}

	public void addRole(String role) {
		this.roles.add(role);
	}

	public String getExtendedInfo() {
		final StringBuilder info = new StringBuilder(this.username).append(":");
		Collections.sort(roles);
		for (String role : roles) {
			info.append(role).append(",");
		}
		info.deleteCharAt(info.length() - 1);
		return info.toString();
	}
}
