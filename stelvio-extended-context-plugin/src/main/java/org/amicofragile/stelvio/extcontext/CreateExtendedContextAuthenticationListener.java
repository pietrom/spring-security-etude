package org.amicofragile.stelvio.extcontext;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;

import org.amicofragile.stelvio.security.management.AuthenticationListener;
import org.amicofragile.stelvio.web.Input;

public class CreateExtendedContextAuthenticationListener implements AuthenticationListener {
	@Override
	public void onAuthenticationSuccess(Input request, String username, Collection<String> roles) throws IOException, ServletException {
		final StelvioExtendedContext context = new StelvioExtendedContext(username);
		for(String role : roles) {
			context.addRole(role);
		}
		request.setSessionAttribute("ExtendedContext", context);
	}
}
