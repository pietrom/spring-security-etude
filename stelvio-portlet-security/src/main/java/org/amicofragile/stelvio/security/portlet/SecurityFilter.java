package org.amicofragile.stelvio.security.portlet;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.filter.ActionFilter;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SecurityFilter implements ActionFilter {
	private static final Logger logger = LoggerFactory.getLogger(SecurityFilter.class);
	@Override
	public void init(FilterConfig filterConfig) throws PortletException {
		logger.info("Init SecurityFilter");
	}

	@Override
	public void destroy() {
		logger.info("Destroy SecurityFilter");
	}

	@Override
	public void doFilter(ActionRequest request, ActionResponse response, FilterChain chain) throws IOException, PortletException {
		logger.info("SecurityFilter.doFilter");
	}
}
