package org.amicofragile.stelvio.security.portlet;

import java.util.Arrays;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.portlet.PortletSecurityException;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.portlet.handler.HandlerInterceptorAdapter;

public class AuthorizationInterceptor extends HandlerInterceptorAdapter {
	private final String[] authorizedRoles;
	
	public AuthorizationInterceptor(String[] authorizedRoles) {
		super();
		this.authorizedRoles = authorizedRoles;
		Arrays.sort(this.authorizedRoles);
	}

	@Override
	protected boolean preHandle(PortletRequest request, PortletResponse response, Object handler) throws Exception {
		SecurityContext ctx = SecurityContextHolder.getContext();
		Authentication auth = ctx.getAuthentication();
		
		if(!isAuthorized(auth)) {
			throw new PortletSecurityException("Not authorized");
		}
		
		return super.preHandle(request, response, handler);
	}

	private boolean isAuthorized(Authentication auth) {
		for (GrantedAuthority authority : auth.getAuthorities()) {
			if(Arrays.binarySearch(this.authorizedRoles, authority.getAuthority()) >= 0) {
				return true;
			}
		}
		return false;
	}
}
