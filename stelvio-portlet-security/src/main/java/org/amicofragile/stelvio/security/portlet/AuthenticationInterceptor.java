package org.amicofragile.stelvio.security.portlet;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;

import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedCredentialsNotFoundException;
import org.springframework.web.portlet.handler.HandlerInterceptorAdapter;

public class AuthenticationInterceptor extends HandlerInterceptorAdapter {
	@Override
	protected boolean preHandle(PortletRequest request, PortletResponse response, Object handler) throws Exception {
		SecurityContext ctx = SecurityContextHolder.getContext();
		if(ctx == null || ctx.getAuthentication() == null || !ctx.getAuthentication().isAuthenticated()) {
			throw new PreAuthenticatedCredentialsNotFoundException("No authentication found");
		}
		return super.preHandle(request, response, handler);
	}
}
