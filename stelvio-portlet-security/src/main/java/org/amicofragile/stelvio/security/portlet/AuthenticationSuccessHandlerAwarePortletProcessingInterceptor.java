package org.amicofragile.stelvio.security.portlet;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

import javax.portlet.PortletRequest;
import javax.portlet.PortletResponse;
import javax.servlet.ServletException;

import org.amicofragile.stelvio.security.management.AuthenticationListener;
import org.amicofragile.stelvio.web.portlet.PortletInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.extensions.portlet.PortletProcessingInterceptor;

public class AuthenticationSuccessHandlerAwarePortletProcessingInterceptor extends PortletProcessingInterceptor {
	private static final Logger logger = LoggerFactory.getLogger(AuthenticationSuccessHandlerAwarePortletProcessingInterceptor.class);
	
	private AuthenticationListener authenticationListener;
	
	@Override
	protected void onSuccessfulAuthentication(PortletRequest request, PortletResponse response, Authentication authentication) throws IOException {
		super.onSuccessfulAuthentication(request, response, authentication);
		if(authenticationListener != null) {
			try {
				authenticationListener.onAuthenticationSuccess(new PortletInput(request), authentication.getName(), buildRoles(authentication));
			} catch (ServletException e) {
				logger.error("Error executing AuthenticationListener", e);
			}
		}
	}
	
	private Collection<String> buildRoles(Authentication authentication) {
		final LinkedList<String> roles = new LinkedList<String>();
		for (GrantedAuthority role : authentication.getAuthorities()) {
			roles.add(role.getAuthority());
		}
		return roles;
	}
	
	public void setAuthenticationListener(AuthenticationListener authenticationListener) {
		this.authenticationListener = authenticationListener;
	}
}
