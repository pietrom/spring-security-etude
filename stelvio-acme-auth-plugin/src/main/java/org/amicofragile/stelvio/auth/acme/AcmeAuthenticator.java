package org.amicofragile.stelvio.auth.acme;

import java.util.Collection;

import org.acme.auth.AuthenticationException;
import org.acme.auth.AuthenticationFacade;
import org.amicofragile.stelvio.auth.AuthenticationFailedException;
import org.amicofragile.stelvio.auth.Authenticator;

public class AcmeAuthenticator implements Authenticator {
	@Override
	public Collection<String> doAuthentication(String username, String password) throws AuthenticationFailedException {
		try {
			return AuthenticationFacade.login(username, password);
		} catch (AuthenticationException e) {
			throw new AuthenticationFailedException("Unable to authenticate " + username + " using ACME Authentication API", e);
		}
	}
}
