package org.amicofragile.stelvio.context;

import java.io.IOException;
import java.util.Collection;

import javax.servlet.ServletException;

import org.amicofragile.stelvio.security.management.AuthenticationListener;
import org.amicofragile.stelvio.web.Input;

public class CreateUserContextAuthenticationListener implements AuthenticationListener {
	@Override
	public void onAuthenticationSuccess(Input request, String username, Collection<String> roles) throws IOException, ServletException {
		final UserContext userContext = new UserContext(username);
		request.setSessionAttribute("UserContext", userContext);
	}
}
