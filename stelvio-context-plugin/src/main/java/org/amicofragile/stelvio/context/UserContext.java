package org.amicofragile.stelvio.context;

public class UserContext {
	private final String username;
	
	public UserContext(String username) {
		this.username = username;
	}
	
	public String getUserInfo() {
		return this.username.toUpperCase();
	}
}
