package org.amicofragile.stelvio.context;

import junit.framework.Assert;

import org.junit.Test;

public class UserContextTest {
	@Test
	public void getUserInfoReturnsUppercasedUsername() {
		final String username = "theUserName";
		final UserContext ctx = new UserContext(username);
		Assert.assertEquals(username.toUpperCase(), ctx.getUserInfo());
	}
}
