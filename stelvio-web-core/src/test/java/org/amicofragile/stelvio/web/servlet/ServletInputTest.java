package org.amicofragile.stelvio.web.servlet;

import javax.servlet.http.HttpSession;

import org.amicofragile.stelvio.web.Input;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpSession;

public class ServletInputTest {
	private static final String KEY = "x-my-header";
	private static final String VALUE = "val-val-val";
	
	private MockHttpServletRequest request;
	private Input input;
	
	@Before
	public void initFixtures() {
		request = new MockHttpServletRequest();
		input = new ServletInput(request);
	}

	@Test
	public void givenHeaderInRequestWhenGetHeaderIsInvokedThenServletInputReturnsHeaderValue() {
		request.addHeader(KEY, VALUE);
		Assert.assertEquals(VALUE, input.getHeader(KEY));
	}
	
	@Test
	public void givenRequestWithoutHeaderWhenGetHeaderIsInvokedThenServletInputReturnsNull() {
		Assert.assertNull(input.getHeader(KEY));
	}
	
	@Test
	public void givenParameterInRequestWhenGetParameterIsInvokedThenServletInputReturnsParameterValue() {
		request.addParameter(KEY, VALUE);
		Assert.assertEquals(VALUE, input.getParameter(KEY));
	}
	
	@Test
	public void givenRequestWithoutParameterWhenGetParameterIsInvokedThenServletInputReturnsNull() {
		Assert.assertNull(input.getParameter(KEY));
	}
	
	@Test
	public void givenAttributeInRequestWhenGetAttributeIsInvokedThenServletInputReturnsAttributeValue() {
		request.setAttribute(KEY, VALUE);
		Assert.assertEquals(VALUE, input.getAttribute(KEY));
	}
	
	@Test
	public void givenRequestWithoutAttributeWhenGetAttributeIsInvokedThenServletInputReturnsNull() {
		Assert.assertNull(input.getAttribute(KEY));
	}
	
	@Test
	public void whenSetAttributeIsInvokedThenServletInputSetsAttributeOnUnderlyingRequest() {
		input.setAttribute(KEY, VALUE);
		Assert.assertEquals(VALUE, request.getAttribute(KEY));
	}
	
	@Test
	public void whenSetSessionAttributeIsInvokedThenServletInputSetsAttributeOnUnderlyingHttpSession() {
		HttpSession session = new MockHttpSession();
		request.setSession(session);
		input.setSessionAttribute(KEY, VALUE);
		Assert.assertEquals(VALUE, session.getAttribute(KEY));
	}
	
	@Test
	public void givenRequestWithoutSessionAttributeWhenGetSessionAttributeIsInvokedThenServletInputReturnsNull() {
		Assert.assertNull(input.getSessionAttribute(KEY));
	}
	
	@Test
	public void givenSessionAttributeInRequestWhenGetSessionAttributeIsInvokedThenServletInputReturnsAttributeValue() {
		HttpSession session = new MockHttpSession();
		session.setAttribute(KEY, VALUE);
		request.setSession(session);
		Assert.assertEquals(VALUE, input.getSessionAttribute(KEY));
	}
}
