package org.amicofragile.stelvio.web.portlet;

import javax.portlet.PortletSession;

import org.amicofragile.stelvio.web.Input;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.mock.web.portlet.MockPortletRequest;
import org.springframework.mock.web.portlet.MockPortletSession;

public class PortletInputTest {
	private static final String KEY = "x-my-header";
	private static final String VALUE = "val-val-val";
	
	private MockPortletRequest request;
	private Input input;
	
	@Before
	public void initFixtures() {
		request = new MockPortletRequest();
		input = new PortletInput(request);
	}

	@Test
	public void givenHeaderInRequestWhenGetHeaderIsInvokedThenPortletInputReturnsHeaderValue() {
		request.addProperty(KEY, VALUE);
		Assert.assertEquals(VALUE, input.getHeader(KEY));
	}
	
	@Test
	public void givenRequestWithoutHeaderWhenGetHeaderIsInvokedThenPortletInputReturnsNull() {
		Assert.assertNull(input.getHeader(KEY));
	}
	
	@Test
	public void givenParameterInRequestWhenGetParameterIsInvokedThenPortletInputReturnsParameterValue() {
		request.addParameter(KEY, VALUE);
		Assert.assertEquals(VALUE, input.getParameter(KEY));
	}
	
	@Test
	public void givenRequestWithoutParameterWhenGetParameterIsInvokedThenPortletInputReturnsNull() {
		Assert.assertNull(input.getParameter(KEY));
	}
	
	@Test
	public void givenAttributeInRequestWhenGetAttributeIsInvokedThenPortletInputReturnsAttributeValue() {
		request.setAttribute(KEY, VALUE);
		Assert.assertEquals(VALUE, input.getAttribute(KEY));
	}
	
	@Test
	public void givenRequestWithoutAttributeWhenGetAttributeIsInvokedThenPortletInputReturnsNull() {
		Assert.assertNull(input.getAttribute(KEY));
	}
	
	@Test
	public void whenSetAttributeIsInvokedThenPortletInputSetsAttributeOnUnderlyingRequest() {
		input.setAttribute(KEY, VALUE);
		Assert.assertEquals(VALUE, request.getAttribute(KEY));
	}
	
	@Test
	public void whenSetSessionAttributeIsInvokedThenPortletInputSetsAttributeOnUnderlyingHttpSession() {
		PortletSession session = new MockPortletSession();
		request.setSession(session);
		input.setSessionAttribute(KEY, VALUE);
		Assert.assertEquals(VALUE, session.getAttribute(KEY, PortletSession.APPLICATION_SCOPE));
	}
	
	@Test
	public void givenRequestWithoutSessionAttributeWhenGetSessionAttributeIsInvokedThenPortletInputReturnsNull() {
		Assert.assertNull(input.getSessionAttribute(KEY));
	}
	
	@Test
	public void givenSessionAttributeInRequestWhenGetSessionAttributeIsInvokedThenPortletInputReturnsAttributeValue() {
		PortletSession session = new MockPortletSession();
		session.setAttribute(KEY, VALUE, PortletSession.APPLICATION_SCOPE);
		request.setSession(session);
		Assert.assertEquals(VALUE, input.getSessionAttribute(KEY));
	}
}
