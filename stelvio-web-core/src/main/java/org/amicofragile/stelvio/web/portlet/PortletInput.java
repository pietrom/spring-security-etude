package org.amicofragile.stelvio.web.portlet;

import javax.portlet.PortletRequest;
import javax.portlet.PortletSession;

import org.amicofragile.stelvio.web.Input;

public class PortletInput implements Input {
	private final PortletRequest request;
	
	public PortletInput(PortletRequest request) {
		this.request = request;
	}

	@Override
	public String getHeader(String key) {
		return request.getProperty(key);
	}

	@Override
	public String getParameter(String key) {
		return request.getParameter(key);
	}

	@Override
	public Object getAttribute(String key) {
		return request.getAttribute(key);
	}

	@Override
	public void setAttribute(String key, Object value) {
		request.setAttribute(key, value);
	}

	@Override
	public void setSessionAttribute(String key, Object value) {
		request.getPortletSession().setAttribute(key, value, PortletSession.APPLICATION_SCOPE);
	}

	@Override
	public Object getSessionAttribute(String key) {
		return request.getPortletSession().getAttribute(key, PortletSession.APPLICATION_SCOPE);
	}
}
