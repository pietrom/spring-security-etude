package org.amicofragile.stelvio.web;

public interface Input {

	public abstract String getHeader(String key);

	public abstract String getParameter(String key);

	public abstract Object getAttribute(String key);

	public abstract void setAttribute(String key, Object value);

	public abstract void setSessionAttribute(String key, Object value);

	public abstract Object getSessionAttribute(String key);
}
