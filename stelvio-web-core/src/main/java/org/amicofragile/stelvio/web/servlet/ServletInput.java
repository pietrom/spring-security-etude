package org.amicofragile.stelvio.web.servlet;

import javax.servlet.http.HttpServletRequest;

import org.amicofragile.stelvio.web.Input;

public class ServletInput implements Input {
	private final HttpServletRequest request;
	public ServletInput(HttpServletRequest request) {
		this.request = request;
	}

	@Override
	public String getHeader(String key) {
		return request.getHeader(key);
	}

	@Override
	public String getParameter(String key) {
		return request.getParameter(key);
	}

	@Override
	public Object getAttribute(String key) {
		return request.getAttribute(key);
	}

	@Override
	public void setAttribute(String key, Object value) {
		request.setAttribute(key, value);
	}

	@Override
	public void setSessionAttribute(String key, Object value) {
		request.getSession().setAttribute(key, value);
	}

	@Override
	public Object getSessionAttribute(String key) {
		return request.getSession().getAttribute(key);
	}
}
